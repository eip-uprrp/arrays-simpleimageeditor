#include "mainwindow.h"
#include <QImage>


///
/// Function that applies a greyscale filter to the edited image.
/// It works by turning each pixel into the color grey. For this we use
/// two for loops to access the pixels of the edited image.
///
void MainWindow::GrayScale(QImage &editedImage){
    // Space to implement the grayscale filter.
    
   // YOUR CODE HERE

}

///
/// Function that applies a vertical flip to the edited image.
/// For this we use two for loops to access the pixels of the images.
/// In the first loop we go through the x axis and in the second we go
/// through the y axis and inside of it we put the pixel from the original
/// image in the heigth-1-j position of the edited image.
///
void MainWindow::VerticalFlip(QImage &editedImage){
    unsigned int width = editedImage.width();
    unsigned int height = editedImage.height();
    unsigned int reverseY;

    QRgb pixel, pixelRev;

    for(unsigned int x = 0; x < width ; x++) {

        for(unsigned int y = 0; y < height / 2; y++){
            reverseY = height - 1 - y;
            pixel    = editedImage.pixel(x,y);
            pixelRev = editedImage.pixel(x,reverseY);

            editedImage.setPixel(x,reverseY,pixel);
            editedImage.setPixel(x,y,pixelRev);
        }
    }
}

///
/// Function that applies a horizontal flip to the edited image
/// For this we use two for loops to access the pixels of the images.
/// In the first loop we go through the x axis and in the second we go
/// through the y axis and inside of it we put the pixel from the original
/// image in the width-1-i position of the edited image.
///
void MainWindow::HorizontalFlip(QImage &editedImage){
    unsigned int width = editedImage.width();
    unsigned int height = editedImage.height();
    unsigned int reverseX;

    QRgb pixel, pixelRev;

    for(unsigned int x = 0; x < width/2; x++) {
        reverseX = width - 1 - x;
        for(unsigned int y = 0; y < height; y++){
            pixel    = editedImage.pixel(x,y);
            pixelRev = editedImage.pixel(reverseX,y);

            editedImage.setPixel(reverseX,y,pixel);
            editedImage.setPixel(x,y,pixelRev);
        }
    }
}

///
/// Function that applies a threshold filter to the edited image.
/// For this we use two for loops to access the pixels of the images.
/// In the first loop we go through the x axis and in the second we go
/// through the y axis.
///
void MainWindow::ThresholdFilter(QImage &originalImage, QImage &editedImage, 
                                 unsigned int threshold, bool invertColor){
    // Space to implement the Threshold Filter.
    // threshold contains the treshold value
    // invertColor is a variable to flip from Black to White for pixel
    // over the threshold and viceversa.
    
    
    // YOUR CODE HERE

 }

